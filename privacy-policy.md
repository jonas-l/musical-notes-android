# Privacy Policy

This application only saves data at your device. It does not transmit it anywhere.

The following is stored:

- your settings
- your highscore
- the last 50 questions
  - the shown note
  - the counter of your mistakes
  - the time until you found the right answer
