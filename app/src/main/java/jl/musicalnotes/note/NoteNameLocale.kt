/*
 * Notes Android - a simple application to train converting written notes to their names
 * Copyright (C) 2019 - 2020 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package jl.musicalnotes.note

import android.content.Context
import android.os.Build
import java.util.*

// this uses the data from https://en.wikipedia.org/w/index.php?title=Musical_note&oldid=958565921#12-tone_chromatic_scale
enum class NoteNameLocale {
    English, German, Dutch, NeoLatin, Japanese, IndianHindustani
}

object NoteNameLocaleUtil {
    private fun formatEnglish(name: NoteName, upperCase: Boolean): String = when (name) {
        NoteName.a -> "a"
        NoteName.b -> "b"
        NoteName.c -> "c"
        NoteName.d -> "d"
        NoteName.e -> "e"
        NoteName.f -> "f"
        NoteName.g -> "g"
    }.let { result ->
        if (upperCase)
            result.toUpperCase(Locale.ENGLISH)
        else
            result
    }

    private fun formatGerman(name: NoteName, upperCase: Boolean): String = when (name) {
        NoteName.b -> if (upperCase) "H" else "h"
        else -> formatEnglish(name, upperCase)
    }

    // different for things like G sharp, but equal for all notes which occur here
    private fun formatDutch(name: NoteName, upperCase: Boolean): String = formatEnglish(name, upperCase)

    private fun formatNeoLatin(name: NoteName): String = when (name) {
        NoteName.a -> "La"
        NoteName.b -> "Si"
        NoteName.c -> "Do"
        NoteName.d -> "Re"
        NoteName.e -> "Mi"
        NoteName.f -> "Fa"
        NoteName.g -> "Sol"
    }

    private fun formatJapanese(name: NoteName): String = when (name) {
        NoteName.a -> "イ"
        NoteName.b -> "ロ"
        NoteName.c -> "ハ"
        NoteName.d -> "ニ"
        NoteName.e -> "ホ"
        NoteName.f -> "ヘ"
        NoteName.g -> "ト"
    }

    private fun formatIndianHindustani(name: NoteName): String = when (name) {
        NoteName.a -> "Dha"
        NoteName.b -> "Ni"
        NoteName.c -> "Sa"
        NoteName.d -> "Re"
        NoteName.e -> "Ga"
        NoteName.f -> "Ma"
        NoteName.g -> "Pa"
    }

    private fun format(name: NoteName, upperCase: Boolean, locale: NoteNameLocale) = when (locale) {
        NoteNameLocale.English -> formatEnglish(name, upperCase)
        NoteNameLocale.German -> formatGerman(name, upperCase)
        NoteNameLocale.Dutch -> formatDutch(name, upperCase)
        NoteNameLocale.NeoLatin -> formatNeoLatin(name)
        NoteNameLocale.Japanese -> formatJapanese(name)
        NoteNameLocale.IndianHindustani -> formatIndianHindustani(name)
    }

    private fun getCountryCodes(context: Context): List<String> = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        context.resources.configuration.locales.let { locales ->
            (0 until locales.size()).map { locales.get(it).isO3Country }.distinct()
        }
    else
        listOf(context.resources.configuration.locale.isO3Country)

    private fun getLocale(context: Context): NoteNameLocale {
        val countries = getCountryCodes(context)

        countries.forEach { country ->
            if (
                country == "AUT" || country == "CZE" || country == "DEU" || country == "DNK" ||
                country == "EST" || country == "FIN" || country == "HUN" || country == "NOR" ||
                country == "POL" || country == "SRB" || country == "SVK" || country == "SVN" ||
                country == "SWE"
            ) {
                return NoteNameLocale.German
            } else if (country == "NLD" || country == "IDN") {
                return NoteNameLocale.Dutch
            } else if (
                country == "ITA" || country == "FRA" || country == "ESP" || country == "ROU" ||
                country == "GRC" || country == "ISR" || country == "TUR" || country == "LVA" ||
                country == "PRT" || country == "ALB" || country == "RUS" || country == "MNG" ||
                country == "SAU" || country == "UKR" || country == "BGR" || country == "VNM"
            ) {
                return NoteNameLocale.NeoLatin
            } else if (country == "JPN") {
                return NoteNameLocale.Japanese
            } else if (country == "IND") {
                return NoteNameLocale.IndianHindustani
            }
        }

        // fallback
        return NoteNameLocale.English
    }

    fun format(name: NoteName, upperCase: Boolean, context: Context) = NoteNameLocaleUtil.format(name, upperCase, getLocale(context))
}