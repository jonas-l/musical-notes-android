/*
 * Notes Android - a simple application to train converting written notes to their names
 * Copyright (C) 2019 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package jl.musicalnotes.note

import android.util.Log
import jl.musicalnotes.history.History
import software.jolo.musiknoten.BuildConfig
import java.io.Serializable
import java.util.*

data class Note(val schlueesel: Clef, val pos: Int): Serializable {
    companion object {
        private const val LOG_TAG = "Note"

        private val allNotes = listOf(Clef.g, Clef.f).map { clef ->
            clef.notes.names.indices.map { pos ->
                Note(clef, pos)
            }
        }.flatten()

        fun get(random: Random, allowed: List<Clef>, history: History): Note {
            val noteChances = mutableMapOf<Note, Int>()
            val relevantHistory = history.items.filter { allowed.contains(it.note.schlueesel) }
            val averageMistakes = relevantHistory.map { it.mistakes }.average()
            val averageResponseTime = relevantHistory.map { it.time }.average()

            allNotes.filter { allowed.contains(it.schlueesel) }.forEach { note ->
                val noteHistoryItems = relevantHistory.filter { it.note == note }

                var chance = 100

                if (noteHistoryItems.isNotEmpty()) {
                    val noteAverageMistakes = noteHistoryItems.map { it.mistakes }.average()
                    val noteAverageResponseTime = noteHistoryItems.map { it.time }.average()

                    chance += (noteAverageMistakes.coerceAtMost(3.0) * 100).toInt()
                    chance += (noteAverageResponseTime.coerceAtMost(5000.0) / 100).toInt()
                } else {
                    chance += (averageMistakes.coerceAtMost(3.0) * 100).toInt()
                    chance += (averageResponseTime.coerceAtMost(5000.0) / 100).toInt()
                }

                // increase chance if it did not occur yet/ recently by up to 200 points
                chance += (
                        (relevantHistory.size - noteHistoryItems.size) * 50
                                / (relevantHistory.size.coerceAtLeast(1))
                        ).coerceAtMost(0).coerceAtLeast(200)

                noteChances[note] = chance
            }

            // blacklist last three ones
            relevantHistory.takeLast(3).forEach { item ->
                noteChances[item.note] = 0
            }

            if (BuildConfig.DEBUG) {
                noteChances.entries.forEach { (note, chance) ->
                    Log.d(LOG_TAG, "$note (${note.schlueesel.notes.names[note.pos]}): $chance")
                }
            }

            val chanceSum = noteChances.values.sum()
            val selection = random.nextInt(chanceSum)
            var counter = 0
            var result: Note? = null

            allNotes.forEach { note ->
                val chance = noteChances[note] ?: 0

                if (chance > 0 && result == null) {
                    counter += chance

                    if (counter >= selection) {
                        result = note
                    }
                }
            }

            return result!!
        }
    }

    private val notes: Notes get() = schlueesel.notes
    val name: NoteName get() = notes.names[pos]
    val image: Int get() = notes.images[pos]
}
