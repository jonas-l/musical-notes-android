/*
 * Notes Android - a simple application to train converting written notes to their names
 * Copyright (C) 2019 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package jl.musicalnotes.note

import software.jolo.musiknoten.R

enum class Clef constructor(val notes: Notes, val imageId: Int) {
    g(
        Notes(
            listOf<NoteName>(
                NoteName.c,
                NoteName.c,
                NoteName.c,
                NoteName.d,
                NoteName.d,
                NoteName.e,
                NoteName.e,
                NoteName.f,
                NoteName.f,
                NoteName.g,
                NoteName.g,
                NoteName.a,
                NoteName.a,
                NoteName.b,
                NoteName.b
            ),
            listOf<Int>(
                R.drawable.n01,
                R.drawable.n02,
                R.drawable.n03,
                R.drawable.n04,
                R.drawable.n05,
                R.drawable.n06,
                R.drawable.n07,
                R.drawable.n08,
                R.drawable.n09,
                R.drawable.n10,
                R.drawable.n11,
                R.drawable.n12,
                R.drawable.n13,
                R.drawable.n14,
                R.drawable.n15
            )
        ),
            R.drawable.violinschlussel),
    f(
        Notes(
            listOf<NoteName>(
                NoteName.c,
                NoteName.c,
                NoteName.c,
                NoteName.d,
                NoteName.d,
                NoteName.e,
                NoteName.e,
                NoteName.f,
                NoteName.f,
                NoteName.g,
                NoteName.g,
                NoteName.a,
                NoteName.a,
                NoteName.b,
                NoteName.b
            ),
            listOf<Int>(
                R.drawable.n12,
                R.drawable.n13,
                R.drawable.n16,
                R.drawable.n14,
                R.drawable.n17,
                R.drawable.n01,
                R.drawable.n02,
                R.drawable.n04,
                R.drawable.n05,
                R.drawable.n06,
                R.drawable.n07,
                R.drawable.n08,
                R.drawable.n09,
                R.drawable.n10,
                R.drawable.n11
            )
        ),
            R.drawable.bassschlussel);

    companion object {
        fun toInt(clef: Clef) = when (clef) {
            g -> 1
            f -> 2
        }

        fun fromInt(index: Int) = when (index) {
            1 -> g
            2 -> f
            else -> throw IllegalArgumentException()
        }
    }
}