/*
 * Notes Android - a simple application to train converting written notes to their names
 * Copyright (C) 2019 - 2020 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package jl.musicalnotes.history

import jl.musicalnotes.note.Clef
import jl.musicalnotes.note.Note
import java.io.StringWriter
import java.util.*

data class HistoryItem(
    val note: Note,
    val time: Long,
    val mistakes: Int
) {
    companion object {
        fun parse(scanner: Scanner): HistoryItem {
            val clef = scanner.nextInt()
            val pos = scanner.nextInt()
            val time = scanner.nextLong()
            val mistakes = scanner.nextInt()

            return HistoryItem(
                note = Note(
                    schlueesel = Clef.fromInt(clef),
                    pos = pos
                ),
                time = time,
                mistakes = mistakes
            )
        }
    }

    fun serialize(writer: StringWriter) {
        writer.append(Clef.toInt(note.schlueesel).toString())
            .append(' ')
            .append(note.pos.toString())
            .append(' ')
            .append(time.toString())
            .append(' ')
            .append(mistakes.toString())
            .append(' ')
    }
}