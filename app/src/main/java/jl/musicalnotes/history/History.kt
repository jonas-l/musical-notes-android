/*
 * Notes Android - a simple application to train converting written notes to their names
 * Copyright (C) 2019 - 2020 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package jl.musicalnotes.history

import java.io.StringWriter
import java.util.*

data class History(val items: List<HistoryItem>) {
    companion object {
        private const val LIST_LENGTH_LIMIT = 50

        fun parse(scanner: Scanner): History {
            val items = mutableListOf<HistoryItem>()

            while (scanner.hasNext()) {
                items.add(HistoryItem.parse(scanner))
            }

            return History(items = items)
        }
    }

    fun serialize(writer: StringWriter) {
        items.forEach { it.serialize(writer) }
    }

    fun appendItem(newItem: HistoryItem): History {
        val newList = (items + listOf(newItem)).takeLast(LIST_LENGTH_LIMIT)

        return History(newList)
    }
}