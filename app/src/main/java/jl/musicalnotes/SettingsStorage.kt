/*
 * Notes Android - a simple application to train converting written notes to their names
 * Copyright (C) 2019 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package jl.musicalnotes

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import jl.musicalnotes.history.History
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import software.jolo.musiknoten.BuildConfig
import java.io.StringReader
import java.io.StringWriter
import java.lang.Exception
import java.util.*

class SettingsStorage (private val sharedPreferences: SharedPreferences) {
    companion object {
        private const val LOG_TAG = "SettingsStorage"

        private const val KEY_ROUNDS = "rounds"
        private const val KEY_CLEF_G = "clefG"
        private const val KEY_CLEF_F = "clefF"
        private const val KEY_HIGH_SCORE = "highScore"
        private const val KEY_HISTORY = "history"

        fun getInstance(context: Context) = SettingsStorage(
            context.applicationContext.getSharedPreferences(
                "config",
                0
            )
        )
    }

    var rounds: Int
        get() = sharedPreferences.getInt(KEY_ROUNDS, 5)
        set(value) {
            sharedPreferences.edit()
                .putInt(KEY_ROUNDS, value)
                .apply()
        }

    private val enableClefGMutable = MutableStateFlow(sharedPreferences.getBoolean(KEY_CLEF_G, true))
    private val enableClefFMutable = MutableStateFlow(sharedPreferences.getBoolean(KEY_CLEF_F, false))
    val enableClefGLive: StateFlow<Boolean> = enableClefGMutable
    val enableClefFLive: StateFlow<Boolean> = enableClefFMutable

    var enableClefG: Boolean
        get() = enableClefGMutable.value
        set(value) {
            enableClefGMutable.value = value

            sharedPreferences.edit()
                .putBoolean(KEY_CLEF_G, value)
                .apply()
        }

    var enableClefF: Boolean
        get() = enableClefFMutable.value
        set(value) {
            enableClefFMutable.value = value

            sharedPreferences.edit()
                .putBoolean(KEY_CLEF_F, value)
                .apply()
        }

    private val highscoreMutable = MutableStateFlow(sharedPreferences.getLong(KEY_HIGH_SCORE, 0))
    val highscoreLive: StateFlow<Long> = highscoreMutable

    var highScore: Long
        get() = highscoreMutable.value
        set(value) {
            highscoreMutable.value = value

            sharedPreferences.edit()
                .putLong(KEY_HIGH_SCORE, value)
                .apply()
        }

    var history: History
        get() = try {
            Scanner(StringReader(sharedPreferences.getString(KEY_HISTORY, "")!!)).use {
                History.parse(it)
            }
        } catch (ex: Exception) {
            if (BuildConfig.DEBUG) {
                Log.d(LOG_TAG, "could not read history", ex)
                Log.d(LOG_TAG, "history: " + sharedPreferences.getString(KEY_HISTORY, ""))
            }

            History(emptyList())
        }
        set(value) {
            sharedPreferences.edit()
                .putString(KEY_HISTORY, StringWriter().apply {
                    value.serialize(this)
                }.buffer.toString())
                .apply()
        }
}