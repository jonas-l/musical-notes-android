/*
 * Notes Android - a simple application to train converting written notes to their names
 * Copyright (C) 2019 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package jl.musicalnotes.activity

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.os.SystemClock
import android.os.Vibrator
import androidx.activity.compose.BackHandler
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.safeDrawing
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.translate
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.fragment.app.FragmentActivity
import jl.musicalnotes.AppTheme
import software.jolo.musiknoten.R
import jl.musicalnotes.SettingsStorage
import jl.musicalnotes.history.HistoryItem
import jl.musicalnotes.note.Note
import jl.musicalnotes.note.Clef
import jl.musicalnotes.note.NoteName
import jl.musicalnotes.note.NoteNameLocaleUtil
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import java.io.Serializable
import java.util.*

class TrainingActivity : FragmentActivity() {
    companion object {
        private const val EXTRA_TYPES = "types"
        private const val EXTRA_ROUNDS = "rounds"
        private const val STATE = "state"

        val random = Random()

        fun launch(context: Context, types: List<Clef>, rounds: Int) {
            context.startActivity(
                    Intent(context, TrainingActivity::class.java)
                            .putExtra(EXTRA_TYPES, ArrayList(types))
                            .putExtra(EXTRA_ROUNDS, rounds)
            )
        }
    }

    private val types: List<Clef> by lazy {
        intent.getSerializableExtra(EXTRA_TYPES) as ArrayList<Clef>
    }

    private val rounds: Int by lazy { intent.getIntExtra(EXTRA_ROUNDS, 0) }

    private val stateLive = MutableStateFlow(State())

    private data class State(
        val counter: Int = 0,
        val visibleNote: Note? = null,
        val startTime: Long = 0,
        val mistakes: Int = 0,
        val score: Int = 0,
        val currentNoteStartTime: Long = 0,
        val currentNoteMistakes: Set<NoteName> = emptySet()
    ): Serializable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        stateLive.value = if (savedInstanceState == null) {
            obtainStartState()
        } else {
            savedInstanceState.getSerializable(STATE) as State
        }

        enableEdgeToEdge()

        val snackbar = SnackbarHostState()

        setContent {
            val state by stateLive.collectAsState()
            var wentBack by remember { mutableStateOf(false) }
            val scope = rememberCoroutineScope()

            val backMessage = stringResource(R.string.press_back_again)

            BackHandler(enabled = !wentBack) {
                scope.launch {
                    wentBack = true

                    snackbar.showSnackbar(backMessage)

                    wentBack = false
                }
            }

            AppTheme {
                Scaffold(
                    snackbarHost = { SnackbarHost(snackbar) },
                    contentWindowInsets = WindowInsets.safeDrawing,
                    content = { insets ->
                        if (state.counter >= rounds) {
                            DoneScreen(
                                insets = insets,
                                score = state.score,
                                exit = ::finish,
                                retry = { stateLive.value = obtainStartState() }
                            )
                        } else {
                            TrainingScreen(
                                insets = insets,
                                state = state
                            )
                        }
                    }
                )
            }
        }
    }

    @Composable
    private fun DoneScreen(
        insets: PaddingValues,
        score: Int,
        exit: () -> Unit,
        retry: () -> Unit
    ) {
        val message = resources.getQuantityString(R.plurals.points, score, score)

        Column (
            Modifier
                .fillMaxSize()
                .padding(insets),
            verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.CenterVertically)
        ) {
            Text(
                message,
                textAlign = TextAlign.Center,
                modifier = Modifier.fillMaxWidth()
            )

            Row(
                Modifier.align(Alignment.CenterHorizontally),
                horizontalArrangement = Arrangement.spacedBy(16.dp)
            ) {
                TextButton(exit) {
                    Text(stringResource(R.string.btn_close))
                }

                Button(retry) {
                    Text(stringResource(R.string.btn_replay))
                }
            }
        }
    }

    @Composable
    private fun TrainingScreen(
        insets: PaddingValues,
        state: State
    ) {
        val isLandscape = LocalConfiguration.current.orientation == Configuration.ORIENTATION_LANDSCAPE

        Column(
            verticalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .fillMaxSize()
                .padding(insets)
        ) {
            LinearProgressIndicator(
                progress = { (state.counter - 1).toFloat() / rounds.toFloat() },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
            )

            if (isLandscape) {
                Row {
                    state.visibleNote?.let { NoteView(it, Modifier.weight(1f)) }

                    NoteInputView(
                        disabledNotes = state.currentNoteMistakes,
                        onClick = ::handleInput,
                        modifier = Modifier.width(IntrinsicSize.Min).fillMaxHeight()
                    )
                }
            } else {
                state.visibleNote?.let { NoteView(it, Modifier.fillMaxWidth()) }

                NoteInputView(
                    disabledNotes = state.currentNoteMistakes,
                    onClick = ::handleInput,
                    modifier = Modifier.fillMaxWidth().height(IntrinsicSize.Min)
                )
            }
        }
    }

    @Composable
    private fun NoteView(note: Note, modifier: Modifier = Modifier) {
        val clefPainter = painterResource(note.schlueesel.imageId)
        val notePainter = painterResource(note.image)

        Image(
            painter = object: Painter() {
                override val intrinsicSize: Size get() =
                    Size(
                        width = clefPainter.intrinsicSize.width
                            .plus(notePainter.intrinsicSize.width),
                        height = clefPainter.intrinsicSize.height
                            .coerceAtLeast(notePainter.intrinsicSize.height)
                    )

                override fun DrawScope.onDraw() {
                    val targetSize = intrinsicSize
                    val actualSize = size
                    val scaleFactor = (actualSize.width / targetSize.width)
                        .coerceAtMost(actualSize.height / targetSize.height)

                    with(clefPainter) {
                        val size = clefPainter.intrinsicSize

                        draw(Size(
                            width = size.width * scaleFactor,
                            height = size.height * scaleFactor
                        ))
                    }

                    with(notePainter) {
                        val size = notePainter.intrinsicSize

                        translate(left = clefPainter.intrinsicSize.width * scaleFactor) {
                            draw(Size(
                                width = size.width * scaleFactor,
                                height = size.height * scaleFactor
                            ))
                        }
                    }
                }
            },
            contentDescription = null,
            modifier = modifier
        )
    }

    @Composable
    private fun NoteInputView(
        disabledNotes: Set<NoteName>,
        onClick: (NoteName) -> Unit,
        modifier: Modifier = Modifier
    ) {
        @Composable
        fun RowScope.Note(name: NoteName?) {
            Button(
                onClick = { name?.let { onClick(it) } },
                content = {
                    name?.let {
                        Text(NoteNameLocaleUtil.format(name, true, LocalContext.current))
                    }
                },
                enabled = name?.let { !disabledNotes.contains(it) } ?: false,
                modifier = Modifier.weight(1.0f).fillMaxHeight()
            )
        }

        @Composable
        fun MyColumn(
            content: @Composable ColumnScope.() -> Unit
        ) {
            Column(
                content = content,
                verticalArrangement = Arrangement.spacedBy(8.dp),
                modifier = modifier.padding(16.dp)
            )
        }

        @Composable
        fun ColumnScope.MyRow(
            content: @Composable RowScope.() -> Unit
        ) {
            Row(
                content = content,
                horizontalArrangement = Arrangement.spacedBy(8.dp),
                modifier = Modifier.weight(1f)
            )
        }

        val isLandscape = LocalConfiguration.current.orientation == Configuration.ORIENTATION_LANDSCAPE

        if (isLandscape) MyColumn {
            MyRow {
                Note(NoteName.c)
                Note(NoteName.d)
            }

            MyRow {
                Note(NoteName.e)
                Note(NoteName.f)
            }

            MyRow {
                Note(NoteName.g)
                Note(NoteName.a)
            }

            MyRow {
                Note(NoteName.b)
                Note(null)
            }
        } else MyColumn {
            MyRow {
                Note(NoteName.c)
                Note(NoteName.d)
                Note(NoteName.e)
                Note(NoteName.f)
            }

            MyRow {
                Note(NoteName.g)
                Note(NoteName.a)
                Note(NoteName.b)
                Note(null)
            }
        }
    }

    public override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putSerializable(STATE, stateLive.value)
    }

    private fun obtainStartState(): State {
        val now = SystemClock.uptimeMillis()
        val settings = SettingsStorage.getInstance(this)

        return State(
            startTime = now,
            visibleNote = Note.get(random, types, settings.history),
            currentNoteStartTime = now
        )
    }

    private fun handleInput(name: NoteName) {
        stateLive.update { oldState ->
            if (oldState.visibleNote == null) oldState
            else if (oldState.visibleNote.name != name) {
                if (oldState.currentNoteMistakes.contains(name)) oldState
                else {
                    val vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

                    vibrator.cancel()
                    vibrator.vibrate(longArrayOf(0, 100, 50, 100), -1)

                    oldState.copy(
                        currentNoteMistakes = oldState.currentNoteMistakes + name,
                        mistakes = oldState.mistakes + 1
                    )
                }
            } else {
                val now = SystemClock.uptimeMillis()
                val settings = SettingsStorage.getInstance(this)

                settings.history = settings.history.appendItem(
                    HistoryItem(
                        note = oldState.visibleNote,
                        mistakes = oldState.currentNoteMistakes.size,
                        time = now - oldState.currentNoteStartTime
                    )
                )

                if (oldState.counter + 1 >= rounds) {
                    var time = SystemClock.uptimeMillis() - oldState.startTime
                    time += (oldState.mistakes * 2000).toLong()
                    time /= 100
                    time = rounds * 50 - time

                    val score = Math.max(time.toInt(), 0)

                    if (score > settings.highScore) {
                        settings.highScore = score.toLong()

                        startActivity(
                            TextActivity.newInstance(
                                "NEW HIGHSCORE",
                                this
                            )
                        )
                    }

                    oldState.copy(
                        counter = oldState.counter + 1,
                        score = score
                    )
                } else {
                    oldState.copy(
                        counter = oldState.counter + 1,
                        visibleNote = Note.get(random, types, settings.history),
                        currentNoteStartTime = now,
                        currentNoteMistakes = emptySet()
                    )
                }
            }
        }
    }
}