/*
 * Notes Android - a simple application to train converting written notes to their names
 * Copyright (C) 2019 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package jl.musicalnotes.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.animation.BounceInterpolator
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.IntOffset
import androidx.fragment.app.FragmentActivity
import jl.musicalnotes.AppTheme
import software.jolo.musiknoten.R
import kotlin.math.roundToInt

class TextActivity : FragmentActivity() {
    companion object {
        private const val EXTRA_TEXT = "text"

        fun newInstance(text: String, context: Context): Intent {
            return Intent(context, TextActivity::class.java).putExtra(EXTRA_TEXT, text)
        }
    }

    private val bounce = BounceInterpolator()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val text = intent.getStringExtra(EXTRA_TEXT) ?: ""

        enableEdgeToEdge()

        setContent {
            var runJumpAnimation by remember { mutableStateOf(false) }

            LaunchedEffect(true) { runJumpAnimation = true }

            val jumpProgress: Float by animateFloatAsState(
                if (runJumpAnimation) 1f else 0f,
                label = "jump",
                animationSpec = tween(2000, 0, LinearEasing)
            )

            val doneVisibility: Float by animateFloatAsState(
                if (runJumpAnimation) 1f else 0f,
                label = "appear",
                animationSpec = tween(1000, 3000)
            )

            val screenHeight = LocalConfiguration.current.screenHeightDp

            AppTheme {
                Scaffold(
                    content = { insets ->
                        Box (
                            Modifier
                                .fillMaxSize()
                                .clickable { finish() }
                        ) {
                            Text(
                                text,
                                style = MaterialTheme.typography.headlineMedium,
                                modifier = Modifier
                                    .align(Alignment.Center)
                                    .padding(insets)
                                    .offset {
                                        val progress = bounce.getInterpolation(jumpProgress) - 1f

                                        IntOffset(0, (progress * screenHeight * 2).roundToInt())
                                    }
                            )

                            Text(
                                stringResource(R.string.tap_to_continue),
                                textAlign = TextAlign.Center,
                                modifier = Modifier
                                    .align(Alignment.BottomStart)
                                    .fillMaxWidth()
                                    .padding(insets)
                                    .alpha(doneVisibility)
                            )
                        }
                    }
                )
            }
        }
    }
}
