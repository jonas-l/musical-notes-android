/*
 * Notes Android - a simple application to train converting written notes to their names
 * Copyright (C) 2019 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package jl.musicalnotes.activity

import android.os.Bundle
import android.text.Spanned
import android.text.style.URLSpan
import android.widget.NumberPicker
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.selection.toggleable
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.text.LinkAnnotation
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.fragment.app.FragmentActivity
import jl.musicalnotes.AppTheme
import jl.musicalnotes.NoClefSelectedDialog
import software.jolo.musiknoten.R
import jl.musicalnotes.SettingsStorage
import jl.musicalnotes.note.Clef
import jl.musicalnotes.note.NoteName
import jl.musicalnotes.note.NoteNameLocaleUtil
import software.jolo.musiknoten.BuildConfig

class MainActivity : FragmentActivity() {
    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val settings = SettingsStorage.getInstance(this)

        enableEdgeToEdge()

        setContent {
            AppTheme {
                var noClefSelectedDialog by remember { mutableStateOf(false) }

                Scaffold(
                    topBar = {
                        TopAppBar(
                            title = {
                                Text(stringResource(R.string.app_name))
                            },
                            colors = TopAppBarDefaults.topAppBarColors(
                                containerColor = MaterialTheme.colorScheme.primaryContainer
                            )
                        )
                    },
                    content = { insets ->
                        Column(
                            Modifier
                                .verticalScroll(rememberScrollState())
                                .padding(insets)
                                .padding(8.dp),
                            verticalArrangement = Arrangement.spacedBy(8.dp)
                        ) {
                            Card(Modifier.fillMaxWidth()) {
                                Column {
                                    Text(
                                        stringResource(R.string.pref_type_title),
                                        style = MaterialTheme.typography.titleLarge,
                                        modifier = Modifier.padding(start = 16.dp, end = 16.dp, top = 16.dp, bottom = 8.dp)
                                    )

                                    ClefCheckbox(
                                        NoteName.f,
                                        settings.enableClefFLive.collectAsState().value,
                                        { settings.enableClefF = it }
                                    )

                                    ClefCheckbox(
                                        NoteName.g,
                                        settings.enableClefGLive.collectAsState().value,
                                        { settings.enableClefG = it }
                                    )
                                }
                            }

                            Card(Modifier.fillMaxWidth()) {
                                Column {
                                    Text(
                                        stringResource(R.string.pref_rounds_title),
                                        style = MaterialTheme.typography.titleLarge,
                                        modifier = Modifier.padding(16.dp)
                                    )

                                    AndroidView(
                                        factory = {
                                            NumberPicker(it).also { np ->
                                                np.minValue = 2
                                                np.maxValue = 99
                                                np.value = settings.rounds
                                                np.setOnValueChangedListener { _, _, value -> settings.rounds = value }
                                            }
                                        },
                                        modifier = Modifier.fillMaxWidth()
                                    )
                                }
                            }

                            Button(
                                modifier = Modifier.fillMaxWidth(),
                                shape = CardDefaults.shape,
                                colors = ButtonDefaults.buttonColors(
                                    containerColor = MaterialTheme.colorScheme.secondaryContainer,
                                    contentColor = MaterialTheme.colorScheme.onSecondaryContainer
                                ),
                                content = {
                                    val highscore by settings.highscoreLive.collectAsState()

                                    Column (Modifier.padding(vertical = 16.dp)) {
                                        Text(
                                            stringResource(R.string.btn_launch),
                                            style = MaterialTheme.typography.titleLarge,
                                            textAlign = TextAlign.Center,
                                            modifier = Modifier.fillMaxWidth()
                                        )

                                        Text(
                                            stringResource(
                                                R.string.current_highscore_text,
                                                LocalContext.current.resources.getQuantityString(
                                                    R.plurals.points, highscore.toInt(), highscore
                                                )
                                            ),
                                            style = MaterialTheme.typography.titleMedium,
                                            textAlign = TextAlign.Center,
                                            modifier = Modifier.fillMaxWidth()
                                        )
                                    }
                                },
                                onClick = {
                                    val clefs = mutableListOf<Clef>()

                                    if (settings.enableClefG) {
                                        clefs.add(Clef.g)
                                    }

                                    if (settings.enableClefF) {
                                        clefs.add(Clef.f)
                                    }

                                    if (clefs.isEmpty()) {
                                        noClefSelectedDialog = true
                                    } else {
                                        TrainingActivity.launch(
                                            this@MainActivity,
                                            clefs,
                                            settings.rounds
                                        )
                                    }
                                }
                            )

                            Card(Modifier.fillMaxWidth()) {
                                Column (
                                    Modifier.padding(16.dp),
                                    verticalArrangement = Arrangement.spacedBy(8.dp)
                                ) {
                                    Text(
                                        stringResource(R.string.license_title),
                                        style = MaterialTheme.typography.titleLarge
                                    )

                                    HyperlinkText(getText(R.string.license_text))
                                }
                            }

                            Card(Modifier.fillMaxWidth()) {
                                Column (
                                    Modifier.padding(16.dp),
                                    verticalArrangement = Arrangement.spacedBy(8.dp)
                                ) {
                                    Text(
                                        stringResource(R.string.about_contained_software_title),
                                        style = MaterialTheme.typography.titleLarge
                                    )

                                    HyperlinkText(getText(R.string.about_contained_software_text))
                                }
                            }

                            Card(Modifier.fillMaxWidth()) {
                                Column (
                                    Modifier.padding(16.dp),
                                    verticalArrangement = Arrangement.spacedBy(8.dp)
                                ) {
                                    Text(
                                        stringResource(R.string.about_source_title),
                                        style = MaterialTheme.typography.titleLarge
                                    )

                                    HyperlinkText(getText(R.string.about_source_text))
                                }
                            }

                            Card(Modifier.fillMaxWidth()) {
                                Column (
                                    Modifier.padding(16.dp),
                                    verticalArrangement = Arrangement.spacedBy(8.dp)
                                ) {
                                    Text(
                                        stringResource(R.string.version_title),
                                        style = MaterialTheme.typography.titleLarge
                                    )

                                    Text(stringResource(R.string.version_text, BuildConfig.VERSION_NAME))
                                }
                            }

                            Card(Modifier.fillMaxWidth()) {
                                Column (
                                    Modifier.padding(16.dp),
                                    verticalArrangement = Arrangement.spacedBy(8.dp)
                                ) {
                                    Text(
                                        stringResource(R.string.about_developer_key_title),
                                        style = MaterialTheme.typography.titleLarge
                                    )

                                    Text(stringResource(R.string.about_developer_key_text))
                                }
                            }
                        }
                    }
                )

                if (noClefSelectedDialog) {
                    NoClefSelectedDialog(
                        onDismissRequest = { noClefSelectedDialog = false }
                    )
                }
            }
        }
    }

    @Composable
    private fun ClefCheckbox(name: NoteName, isChecked: Boolean, setChecked: (Boolean) -> Unit) {
        Row(
            Modifier
                .fillMaxWidth()
                .height(56.dp)
                .toggleable(
                    value = isChecked,
                    onValueChange = { setChecked(!isChecked) },
                    role = Role.Checkbox
                ).padding(horizontal = 16.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Checkbox(
                checked = isChecked,
                onCheckedChange = null
            )

            Text (
                text = stringResource(
                    R.string.pref_clef_s,
                    NoteNameLocaleUtil.format(name, true, LocalContext.current)
                ),
                style = MaterialTheme.typography.bodyLarge,
                modifier = Modifier.padding(start = 16.dp)
            )
        }
    }

    @Composable
    private fun HyperlinkText(text: CharSequence) {
        Text(buildAnnotatedString {
            append(text)

            if (text is Spanned) {
                text.getSpans(0, length, URLSpan::class.java).forEach { span ->
                    span as URLSpan

                    val start = text.getSpanStart(span)
                    val end = text.getSpanEnd(span)

                    addStyle(
                        SpanStyle(
                            textDecoration = TextDecoration.Underline
                        ),
                        start, end
                    )

                    addLink(
                        LinkAnnotation.Url(span.url),
                        start, end
                    )
                }
            }
        }
        )
    }
}
