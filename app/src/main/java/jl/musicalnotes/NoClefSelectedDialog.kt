/*
 * Notes Android - a simple application to train converting written notes to their names
 * Copyright (C) 2019 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package jl.musicalnotes

import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import software.jolo.musiknoten.R

@Composable
fun NoClefSelectedDialog(onDismissRequest: () -> Unit) {
    AlertDialog(
        title = {
            Text(stringResource(R.string.no_clef_title))
        },
        text = {
            Text(stringResource(R.string.no_clef_text))
        },
        confirmButton = {
            TextButton(
                onClick = onDismissRequest,
                content = {
                    Text(stringResource(android.R.string.ok))
                }
            )
        },
        onDismissRequest = onDismissRequest
    )
}