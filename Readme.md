## Musical notes

A simple application to train converting written notes to their names.

![Screenshot of TrainingActivity](./app/src/main/play/listings/en-US/graphics/phone-screenshots/2.png)

As the image suggests, you see a image of a note and you have to select the correct name.
If you selected the wrong name, then the button is disabled and you can guess again.

When doing this, you get a (high)score which is calculated using the time and the number of the mistakes.

### Background

This is a improved version of an App which I never published because it was to bad:

- low quality (scanned) graphics
- no Kotlin code (because that was not common at this time)

There are no plans for further improvments. If someones has got a idea which I like,
then I accept pull requests. Otherwise, there are no changes to expect.

### Graphics

(Files for that are in the folder ``assets``)

The notes and clefs were rendered using [LilyPond](http://lilypond.org/) to a PDF.
This PDF was converted to an image and cut using the [GNU Image Manipulation Program](https://www.gimp.org/).

### Localisation

The note names are not equal everywhere, so <https://en.wikipedia.org/w/index.php?title=Musical_note&oldid=958565921#12-tone_chromatic_scale>
was used to localize them.

### Download

<https://f-droid.org/en/packages/jl.musicalnotes/>

### License

GPL-3.0-only
