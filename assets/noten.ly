\version "2.18.2"

melody = \relative c' {
  \time 100/4
  \clef "bass"
  c
  \clef "violin"
  a4 b c d e f g a b c d e f g a b c d
}

\score {
  <<
    \new Staff { \melody }
  >>
  \layout { }
}
